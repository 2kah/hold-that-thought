﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundEffect
{
    Am, C, D, Em, F, G
}

public class AudioSourcePool : MonoBehaviour
{
    public GameObject AudioSrcPrefab;

    public int minPoolSize, maxPoolSize;
    public AnimationCurve poolSizeRamp;

    public List<AudioClip> clips = new List<AudioClip>();

    List<AudioSource> pool;
    GameObject container;
    GameEnder gameEnder;

    // Start is called before the first frame update
    void Start()
    {
        gameEnder = FindObjectOfType<GameEnder>();

        ResetPool();
    }

    public void ResetPool()
    {
        Destroy(container);

        container = new GameObject();
        container.transform.parent = transform;

        pool = new List<AudioSource>(maxPoolSize);

        AddToPool(minPoolSize);
    }

    void AddToPool(int num)
    {
        for(var i = 0; i < num; i++)
        {
            pool.Add(Instantiate(AudioSrcPrefab, container.transform).GetComponent<AudioSource>());
        }
    }

    public void PlayClip(SoundEffect effect)
    {
        var clip = clips[(int)effect];
        PlayClip(clip);
    }

    public void PlayClip(AudioClip clip)
    {
        var src = GetPooledSrc();
        if(src != null)
        {
            src.clip = clip;
            src.Play();
        }
    }

    AudioSource GetPooledSrc()
    {
        for(var i = 0; i < pool.Count; i++)
        {
            if(!pool[i].isPlaying) { return pool[i]; }
        }

        return null;
    }

    // Update is called once per frame
    void Update()
    {
        var progress = (float) PersuadedCounter.Count / (float) gameEnder.persuadedRequirement;
        var curveSample = poolSizeRamp.Evaluate(progress);
        var targetPoolSize = Mathf.RoundToInt(Mathf.Lerp(minPoolSize, maxPoolSize, curveSample));

        if(pool.Count < targetPoolSize)
        {
            AddToPool(targetPoolSize - pool.Count);
        }
    }
}
