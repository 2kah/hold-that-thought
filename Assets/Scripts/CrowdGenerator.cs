﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdGenerator : MonoBehaviour
{
    public GameObject personPrefab;

    public int crowdSize;
    public float spawnRadius;

    public int numAttractors;
    public List<Vector2> Attractors = new List<Vector2>();

    GameObject container;

    public void Spawn()
    {
        // Create attractors
        for(var i = 0; i < numAttractors; i++)
        {
            Attractors.Add(Random.insideUnitCircle * spawnRadius);
        }

        // Create container for persons
        container = new GameObject();
        container.transform.parent = transform;

        for(var i = 0; i < crowdSize; i++)
        {
            var spawnPoint = Random.insideUnitCircle * spawnRadius;
            var movement = Instantiate(personPrefab, spawnPoint, Quaternion.identity, container.transform).GetComponent<PersonMovement>();
            movement.Attractors = Attractors;
        }
    }

    public void DestroyCrowd()
    {
        Destroy(container);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
