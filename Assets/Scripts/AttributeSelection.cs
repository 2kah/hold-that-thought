﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributeSelection : MonoBehaviour
{
    public int pointsToSpend;

    public Text pointsTotalText;

    public Action NoPointsLeft, PointsReturned, OnReset;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointsChanged(int delta)
    {
        bool werePointsLeft = pointsToSpend > 0;

        pointsToSpend -= delta;
        pointsTotalText.text = "Points To Spend: " + pointsToSpend.ToString();

        if(pointsToSpend <= 0) { NoPointsLeft.Invoke(); }
        if(!werePointsLeft && pointsToSpend > 0) { PointsReturned.Invoke(); }
    }

    public void ResetAttributes()
    {
        OnReset.Invoke();
    }
}
