﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributeSelector : MonoBehaviour
{
    public int Value, Min, Max;

    public Text valueText;
    public Button decreaseButton, increaseButton;

    public Action<int> OnValueChanged;

    AttributeSelection attributeSelection;

    // Start is called before the first frame update
    void Start()
    {
        attributeSelection = FindObjectOfType<AttributeSelection>();

        attributeSelection.NoPointsLeft += () => { increaseButton.interactable = false; };
        attributeSelection.PointsReturned += () => { increaseButton.interactable = Value < Max; };
        attributeSelection.OnReset += DoReset;

        increaseButton.interactable = Value < Max;
        decreaseButton.interactable = Value > Min;

        valueText.text = Value.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void DoReset()
    {
        ChangeValue(Min - Value);
    }

    public void ChangeValue(int delta)
    {
        Value += delta;
        Value = Mathf.Clamp(Value, Min, Max);

        increaseButton.interactable = Value < Max;
        decreaseButton.interactable = Value > Min;

        valueText.text = Value.ToString();

        attributeSelection.OnPointsChanged(delta);
        OnValueChanged?.Invoke(Value);
    }
}
