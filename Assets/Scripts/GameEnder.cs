﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEnder : MonoBehaviour
{
    public int persuadedRequirement;

    Persuasion playerPersuasion;

    public GameObject gameEndPanel;
    public Text gameEndText, subText;
    public HUD hud;
    public AttributeSelector timeSelector;
    public Text requirementText;

    public float timeLimit;

    public float startTime;

    float initialTimeLimit;
    int initialPersuadedRequirement;
    bool timeWarningProvided = false;

    AudioSource audioSrc;
    public AudioClip timeWarningSound, winSound, loseSound;

    // Start is called before the first frame update
    void Start()
    {
        initialTimeLimit = timeLimit;
        initialPersuadedRequirement = persuadedRequirement;
        playerPersuasion = GetComponentInChildren<Persuasion>();
        timeSelector.OnValueChanged += ChangeTimeLimit;
        requirementText.text = $"Persuade {persuadedRequirement} people to win";

        audioSrc = GetComponent<AudioSource>();
    }

    public void ResetState()
    {
        startTime = Time.time;
        timeWarningProvided = false;
    }

    public void ChangeRequirement(int points)
    {
        persuadedRequirement = initialPersuadedRequirement - (points * 10);
        requirementText.text = $"Persuade {persuadedRequirement} people to win";
        hud.RequirementChanged();
    }

    void ChangeTimeLimit(int points)
    {
        timeLimit = initialTimeLimit + (points * 10f);
    }

    // Update is called once per frame
    void Update()
    {
        if(gameEndPanel.activeInHierarchy) { return; }

        if(!timeWarningProvided && timeLimit - (Time.time - startTime) <= 10f)
        {
            hud.FlashTimer();
            StartCoroutine(PlayTimeWarningSound());
            timeWarningProvided = true;
        }

        if (playerPersuasion.totalPersuasion < 0.5f || Time.time - startTime >= timeLimit)
        {
            EndGame(false);
        }
        else if (PersuadedCounter.Count >= persuadedRequirement)
        {
            EndGame(true);
        }
    }

    IEnumerator PlayTimeWarningSound()
    {
        audioSrc.clip = timeWarningSound;
        audioSrc.Play();
        yield return new WaitForSeconds(1f);
        audioSrc.Play();
    }

    void EndGame(bool won)
    {
        Time.timeScale = 0f;

        if(won)
        {
            audioSrc.clip = winSound;
            audioSrc.Play();

            gameEndText.text = "Victory!";
            subText.text = "You have kept your thought alive and soon the whole world will be convinced";

            // Ensure the HUD is correct
            hud.totalPersuadedBar.value = persuadedRequirement;
            hud.totalPersuadedText.text = persuadedRequirement.ToString();
        }
        else
        {
            audioSrc.clip = loseSound;
            audioSrc.Play();

            gameEndText.text = "Failure";
            subText.text = "You failed to keep your thought alive and everyone thinks you're stupid and a bit weird";
        }

        gameEndPanel.SetActive(true);
    }
}
