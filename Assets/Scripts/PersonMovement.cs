﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonMovement : MonoBehaviour
{
    public float speed;
    public float additiveSpeedRange;

    public List<Vector2> Attractors;

    Rigidbody2D rb2d;
    float startTime = 0f, walkDuration = 0f;
    Vector2 direction;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        speed += Random.Range(-additiveSpeedRange, additiveSpeedRange);
    }

    void FixedUpdate()
    {
        if (Time.time - startTime >= walkDuration)
        {
            walkDuration = Random.Range(1f, 6f);

            var randIdx = Random.Range(0, Attractors.Count);

            Vector2 pos2D = transform.position;
            direction = Attractors[randIdx] - pos2D;
            
            startTime = Time.time;
        }

        rb2d.AddForce(direction.normalized * speed);
    }
}
