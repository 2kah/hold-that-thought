﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Text totalPersuadedText;
    public Slider totalPersuadedBar;

    public Slider playerPersuasionBar;
    public Image playerPersuasionFill;

    public Text timeText;

    public Persuasion playerPersuasion;

    GameEnder gameEnder;
    ColourPalette colourPalette;

    // Start is called before the first frame update
    void Start()
    {
        gameEnder = FindObjectOfType<GameEnder>();
        colourPalette = FindObjectOfType<ColourPalette>();

        totalPersuadedBar.maxValue = gameEnder.persuadedRequirement;
    }

    public void RequirementChanged()
    {
        totalPersuadedBar.maxValue = gameEnder.persuadedRequirement;
    }

    // Update is called once per frame
    void Update()
    {
        totalPersuadedText.text = PersuadedCounter.Count.ToString();
        totalPersuadedBar.value = PersuadedCounter.Count;

        playerPersuasionBar.value = playerPersuasion.totalPersuasion - 0.5f;
        if(playerPersuasion.totalPersuasion < 0.7f) { playerPersuasionFill.color = colourPalette.Low; }
        else if(playerPersuasion.totalPersuasion < 0.9f) { playerPersuasionFill.color = colourPalette.Medium; }
        else { playerPersuasionFill.color = colourPalette.High; }

        var timespan = TimeSpan.FromSeconds(gameEnder.timeLimit - (Time.time - gameEnder.startTime));
        timeText.text = timespan.ToString(@"mm\:ss");
    }

    public void FlashTimer()
    {
        StartCoroutine(DoTimerFlash());
    }

    IEnumerator DoTimerFlash()
    {
        var flashTime = 0.5f;
        var initialColour = timeText.color;

        timeText.color = Color.red;
        yield return new WaitForSeconds(flashTime);
        timeText.color = initialColour;
        yield return new WaitForSeconds(flashTime);
        timeText.color = Color.red;
        yield return new WaitForSeconds(flashTime);
        timeText.color = initialColour;
    }
}
