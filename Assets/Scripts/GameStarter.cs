﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStarter : MonoBehaviour
{
    public GameObject endPanel, startPanel;
    public Persuasion playerPersuasion;

    CrowdGenerator crowd;
    GameEnder gameEnder;
    AttributeSelection attributeSelection;
    AudioSourcePool audioPool;
    PlayerMovement playerMovement;

    public AttributeSelector charismaSelector, confidenceSelector, timeSelector, totalSelector;

    Random.State randState;

    // Start is called before the first frame update
    void Start()
    {
        crowd = FindObjectOfType<CrowdGenerator>();
        gameEnder = FindObjectOfType<GameEnder>();
        attributeSelection = FindObjectOfType<AttributeSelection>();
        audioPool = FindObjectOfType<AudioSourcePool>();
        playerMovement = FindObjectOfType<PlayerMovement>();

        confidenceSelector.OnValueChanged += UpdatePlayerConfidence;
        UpdatePlayerConfidence(confidenceSelector.Value);

        totalSelector.OnValueChanged += gameEnder.ChangeRequirement;

        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void UpdatePlayerConfidence(int conf)
    {
        playerPersuasion.UpdateConfidence(confidenceSelector.Value / 10f);
    }

    public void StartGame()
    {
        randState = Random.state;

        playerPersuasion.charisma = charismaSelector.Value / 10f;

        crowd.Spawn();

        startPanel.SetActive(false);

        gameEnder.startTime = Time.time;

        Time.timeScale = 1f;
    }

    void ResetGame()
    {
        playerPersuasion.totalPersuasion = 1f;
        PersuadedCounter.Count = 1;
        gameEnder.ResetState();
        crowd.DestroyCrowd();
        audioPool.ResetPool();
        playerMovement.ResetPosition();
    }

    void ResetAttributes()
    {
        attributeSelection.ResetAttributes();
    }

    public void ReturnToStartScreen()
    {
        ResetAttributes();

        endPanel.SetActive(false);
        startPanel.SetActive(true);

        ResetGame();
    }

    public void Retry()
    {
        ResetGame();
        Random.state = randState;

        endPanel.SetActive(false);
        StartGame();
    }
}
