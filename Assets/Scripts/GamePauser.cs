﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePauser : MonoBehaviour
{
    public GameObject pausePanel, startPanel, endPanel;

    public Slider volumeSlider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Game is not in progress
        if(startPanel.activeInHierarchy || endPanel.activeInHierarchy)
        {
            return;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            pausePanel.SetActive(!pausePanel.activeInHierarchy);
            Time.timeScale = Time.timeScale == 0f ? 1f : 0f;
        }
    }

    public void Resume()
    {
        pausePanel.SetActive(false);
        Time.timeScale = 1f;
    }

    public void OnVolumeSliderChange(float val)
    {
        AudioListener.volume = val / 10f;
    }
}
