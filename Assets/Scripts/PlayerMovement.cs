﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;

    Rigidbody2D rb2d;


    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if(Time.timeScale == 0f) { return; }

        Vector2 movement = Vector2.zero;

        if (Input.GetMouseButton(0))
        {
            var mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            movement = Vector2.ClampMagnitude(mousePos - transform.position, 1f);
        }

        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        //horizontal = horizontal == 0f ? -rb2d.velocity.x : horizontal;
        //vertical = vertical == 0f ? -rb2d.velocity.y : vertical;

        // Keyboard higher priority
        if (horizontal != 0f || vertical != 0f)
        {
            movement = new Vector2(horizontal, vertical).normalized;
        }

        rb2d.AddForce(movement * speed);
        //rb2d.velocity = movement.normalized * speed;
    }

    public void ResetPosition()
    {
        rb2d.MovePosition(Vector2.zero);
        rb2d.velocity = Vector2.zero;
    }
}
