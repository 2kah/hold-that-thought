﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonColour : MonoBehaviour
{
    SpriteRenderer sr;
    Persuasion persuasion;
    ColourPalette colourPalette;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        persuasion = GetComponentInChildren<Persuasion>();
        colourPalette = FindObjectOfType<ColourPalette>();
    }

    void Update()
    {
        if (persuasion.totalPersuasion < 0.5f)
        {
            if (sr.color != colourPalette.None) { sr.color = colourPalette.None; }
        }
        else if (persuasion.totalPersuasion < 0.7f)
        {
            if (sr.color != colourPalette.Low) { sr.color = colourPalette.Low; }
        }
        else if (persuasion.totalPersuasion < 0.9f)
        {
            if (sr.color != colourPalette.Medium) { sr.color = colourPalette.Medium; }
        }
        else
        {
            if (sr.color != colourPalette.High) { sr.color = colourPalette.High; }
        }
    }
}
