﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Persuasion : MonoBehaviour
{
    public float totalPersuasion;

    public float skepticism;    // Decreases rate of persuasion
    public float charisma;      // Increases rate of persuading others
    public float confidence;    // Determines range of persuading others

    public bool isPlayer;

    float minRadius = 0.3f, maxRadius = 0.7f;

    float persuasionThreshold = 0.5f;
    float decayMultiplier = 0.0004f;

    CircleCollider2D collider2d;
    AudioSourcePool audioPool;

    // Start is called before the first frame update
    void Start()
    {
        if(!isPlayer)
        {
            skepticism = Random.value;
            charisma = Random.value;
            confidence = Random.value;

            collider2d = GetComponent<CircleCollider2D>();
            collider2d.radius = Mathf.Lerp(minRadius, maxRadius, confidence);
        }

        audioPool = FindObjectOfType<AudioSourcePool>();
    }

    public void UpdateConfidence(float val)
    {
        // This is only used for the player as the player is a bit special
        if(!isPlayer) { return; }

        confidence = Mathf.Clamp01(val);
        var scale = Mathf.Lerp(3f, 5f, val);
        transform.localScale = new Vector3(scale, scale);
    }

    // Update is called once per frame
    void Update()
    {
        // Decay persuasion over time, faster the more people are persuaded
        var prevPersuasion = totalPersuasion;
        totalPersuasion -= Time.deltaTime * decayMultiplier * (float)PersuadedCounter.Count;
        totalPersuasion = Mathf.Clamp01(totalPersuasion);

        if(prevPersuasion >= persuasionThreshold && totalPersuasion < persuasionThreshold)
        {
            OnLostPersuasion();
        }
    }

    public void OnBecamePersuaded()
    {
        PersuadedCounter.Count++;

        collider2d.enabled = true;
    }

    public void OnLostPersuasion()
    {
        if(Time.timeScale == 0f) { return; }

        PersuadedCounter.Count--;

        if (!isPlayer)
        {
            collider2d.enabled = false;
            audioPool.PlayClip(SoundEffect.Am);
        }
        else
        {
            audioPool.PlayClip(SoundEffect.Em);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        // This function controls pursuading others

        // If we are not persuaded ourselves, or we've just hit another confidence zone then stop
        if(totalPersuasion < persuasionThreshold || other.tag == "Confidence") { return; }

        var otherPersuasion = other.GetComponentInChildren<Persuasion>();

        var prevPersuasion = otherPersuasion.totalPersuasion;
        otherPersuasion.totalPersuasion += PersuasionGain(otherPersuasion, Time.deltaTime);
        totalPersuasion = Mathf.Clamp01(totalPersuasion);

        // Check if current conversation has persuaded other person
        if (prevPersuasion < persuasionThreshold && otherPersuasion.totalPersuasion >= persuasionThreshold)
        {
            otherPersuasion.OnBecamePersuaded();
            if(isPlayer)
            {
                // Player persuaded someone
                audioPool.PlayClip(SoundEffect.C);
            }
            else
            {
                audioPool.PlayClip(Random.value < 0.5f ? SoundEffect.F : SoundEffect.G);
            }
        }
    }

    float PersuasionGain(Persuasion person, float time)
    {
        return time * 2f * charisma * (1f - person.skepticism);
    }
}
